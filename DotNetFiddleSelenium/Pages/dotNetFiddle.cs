﻿using System;
using OpenQA.Selenium;

public class DotNetFiddle
{
	public String url;
	public By runButton;
	public By outputResults;
	public By processingRun;
	public By saveButton;
	public By loginMessage;
	public By emailLogin;

	public DotNetFiddle()
	{
		url = "https://dotnetfiddle.net/";
		runButton = By.Id("run-button");
		outputResults = By.Id("output");
		processingRun = By.ClassName("spin");
		saveButton = By.Id("save-button");
		loginMessage = By.Id("login-message");
		emailLogin = By.Id("Email");
	}
}
