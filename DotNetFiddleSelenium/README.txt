﻿Prerequisites:
A) Visual Studio 2019 (community edition is fine)
B) Chrome should be installed (x64 was utilized for this)

To run:
1) Clone repository
2) Ensure all packages are installed (Should already be)
3) Right click "[TestFixture]" in the class
4) From the context menu click "Run Test(s)"