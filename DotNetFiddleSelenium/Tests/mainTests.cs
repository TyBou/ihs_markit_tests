﻿using System;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;

namespace DotNetFiddleSeleniumTests
{
    [TestFixture]
    public class MainTestClass
    {
        static IWebDriver chromeDriver;
        static DotNetFiddle dotNetFiddlePage;
        static WebDriverWait wait;

        [OneTimeSetUp]
        public void SetupBeforeAll()
        {
            chromeDriver = new ChromeDriver();
            dotNetFiddlePage = new DotNetFiddle();

            chromeDriver.Manage().Window.Maximize();
            chromeDriver.Navigate().GoToUrl(dotNetFiddlePage.url);
            chromeDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(15);
            wait = new WebDriverWait(chromeDriver, TimeSpan.FromSeconds(15));
        }

        [OneTimeTearDown]
        public void TeardownAfterAll()
        {
            chromeDriver.Dispose();
        }

        [Test]
        public void Test1()
        {
            String expectedRunResult = "Hello World";

            chromeDriver.FindElement(dotNetFiddlePage.runButton).Click();
            wait.Until(ExpectedConditions.ElementIsVisible(dotNetFiddlePage.processingRun));
            wait.Until(ExpectedConditions.InvisibilityOfElementLocated(dotNetFiddlePage.processingRun));
            Assert.AreEqual(expectedRunResult, chromeDriver.FindElement(dotNetFiddlePage.outputResults).Text);
        }

        [Test]
        public void Test2()
        {
            chromeDriver.FindElement(dotNetFiddlePage.saveButton).Click();
            wait.Until(ExpectedConditions.ElementExists(dotNetFiddlePage.loginMessage));
            IWebElement loginInput = wait.Until(ExpectedConditions.ElementIsVisible(dotNetFiddlePage.emailLogin));
            Assert.IsTrue(loginInput.Displayed);
        }
    }
}
